package com.epam;

import org.junit.Test;
import org.junit.Assert;

public class FormulaGTest {
    @Test
    public void testCalcG_withValues1() {
	double G = FormulaG.calcG(1, 1, 1, 1);
	Assert.assertEquals(G, 19.739208802178716, 0.0000000001);
    }

    @Test
    public void testCalcG_withNegativeValues1() {
	double G = FormulaG.calcG(-1, -1, -1, -1);
	Assert.assertEquals(G, 19.739208802178716, 0.0000000001);
    }

    @Test
    public void testCalcG_withNegativeAnswear() {
	double G = FormulaG.calcG(-1, 1, 1, 1);
	Assert.assertEquals(G, -19.739208802178716, 0.0000000001);
    }

    @Test
    public void testCalcG_withAnswear0() {
	double G = FormulaG.calcG(0, 1, 1, 1);
	Assert.assertEquals(G, 0.0, 0.0000000001);
    }

    @Test
    public void testCalcG_withValues1234() {
	double G = FormulaG.calcG(1, 2, 3, 4);
	Assert.assertEquals(G, 1.4099434858699083, 0.0000000001);
    }
}
