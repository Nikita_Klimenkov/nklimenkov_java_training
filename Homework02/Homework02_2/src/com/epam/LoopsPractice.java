package com.epam;

public class LoopsPractice {

    public static int[] calcFibonacci(int loopType, int n) {
	int[] fibArray = new int[n];
	fibArray[0] = 1;
	if (n == 1) {
	    return fibArray;
	}
	fibArray[1] = 1;
	if (n == 2) {
	    return fibArray;
	}
	int i = 2;
	switch (loopType) {
	    case 1: {
		while (i < n) {
		    fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
		    i++;
		}
		break;
	    }
	    case 2: {
		do {
		    fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
		    i++;
		} while (i < n);
		break;
	    }
	    case 3: {
		for (i = 2; i < n; i++) {
		    fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
		}
		break;
	    }
	}
	return fibArray;
    }

    public static int calcFactorial(int loopType, int n) {
	int factorial = 1;
	if (n == 0 || n == 1) {
	    return factorial;
	}
	int i = 2;
	switch (loopType) {
	    case 1: {
		while (i <= n) {
		    factorial *= i;
		    i++;
		}
		break;
	    }
	    case 2: {
		do {
		    factorial *= i;
		    i++;
		} while (i <= n);
		break;
	    }
	    case 3: {
		for (i = 1; i <= n; i++) {
		    factorial *= i;
		}
		break;
	    }
	}
	return factorial;
    }

    public static void main(String[] args) {
	int algorithmId = Integer.parseInt(args[0]);
	int loopType = Integer.parseInt(args[1]);
	int n = Integer.parseInt(args[2]);
	
	if (loopType != 1 && loopType != 2 && loopType != 3) {
	    System.out.println("Loop type was entered incorrectly :(");
	    return;
	}
	switch (algorithmId) {
	    case 1:
		if (n == 0) {
		    System.out.println("You entered '0' as a parametr n so " + 
			"you won't get any Fibonacci numbers :(");
		    return;
		} else if (n < 0) {
		    System.out.println("In this algorithm parameter n must be >0");
		    return;
		}
		int[] fibArray = calcFibonacci(loopType, n);		
		System.out.print("First " + n + " Fibonacci numbers are: ");
		for (int i = 0; i < n; i++) {
		    System.out.print(fibArray[i] + " ");
		}
		break;		
	    case 2:
		if (n < 0) {
		    System.out.print("In this algorithm parameter n must be >=0");
		    return;
		}
		int factorial = calcFactorial(loopType, n);
		System.out.println(n + "! = " + factorial);
		break;
	    default:
		System.out.println("Algorithm Id was entered incorrectly :(");
	}
    }
}
