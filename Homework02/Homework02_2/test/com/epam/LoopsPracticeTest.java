package com.epam;

import org.junit.Test;
import org.junit.Assert;

public class LoopsPracticeTest {
    @Test
    public void calcFibonacciTest_whenLoopWhileValue1() {
	int[] fibArray = LoopsPractice.calcFibonacci(1, 1);
	int[] answer = { 1 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFibonacciTest_whenLoopWhileValue2() {
	int[] fibArray = LoopsPractice.calcFibonacci(1, 2);
	int[] answer = { 1, 1 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFibonacciTest_whenLoopWhileValue10() {
	int[] fibArray = LoopsPractice.calcFibonacci(1, 10);
	int[] answer = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFibonacciTest_whenLoopDoWhileValue1() {
	int[] fibArray = LoopsPractice.calcFibonacci(2, 1);
	int[] answer = { 1 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFibonacciTest_whenLoopDoWhileValue2() {
	int[] fibArray = LoopsPractice.calcFibonacci(2, 2);
	int[] answer = { 1, 1 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFibonacciTest_whenLoopDoWhileValue10() {
	int[] fibArray = LoopsPractice.calcFibonacci(2, 10);
	int[] answer = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFibonacciTest_whenLoopForValue1() {
	int[] fibArray = LoopsPractice.calcFibonacci(3, 1);
	int[] answer = { 1 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFibonacciTest_whenLoopForValue2() {
	int[] fibArray = LoopsPractice.calcFibonacci(3, 2);
	int[] answer = { 1, 1 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFibonacciTest_whenLoopForValue10() {
	int[] fibArray = LoopsPractice.calcFibonacci(3, 10);
	int[] answer = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };
	Assert.assertArrayEquals(answer, fibArray);
    }

    @Test
    public void calcFactorialTest_whenLoopWhileValue0() {
	int factorial = LoopsPractice.calcFactorial(1, 0);
	int answer = 1;
	Assert.assertEquals(answer, factorial);
    }

    @Test
    public void calcFactorialTest_whenLoopWhileValue1() {
	int factorial = LoopsPractice.calcFactorial(1, 1);
	int answer = 1;
	Assert.assertEquals(answer, factorial);
    }

    @Test
    public void calcFactorialTest_whenLoopWhileValue5() {
	int factorial = LoopsPractice.calcFactorial(1, 5);
	int answer = 120;
	Assert.assertEquals(answer, factorial);
    }

    @Test
    public void calcFactorialTest_whenLoopDoWhileValue0() {
	int factorial = LoopsPractice.calcFactorial(2, 0);
	int answer = 1;
	Assert.assertEquals(answer, factorial);
    }

    @Test
    public void calcFactorialTest_whenLoopDoWhileValue1() {
	int factorial = LoopsPractice.calcFactorial(2, 1);
	int answer = 1;
	Assert.assertEquals(answer, factorial);
    }

    @Test
    public void calcFactorialTest_whenLoopDoWhileValue5() {
	int factorial = LoopsPractice.calcFactorial(2, 5);
	int answer = 120;
	Assert.assertEquals(answer, factorial);
    }

    @Test
    public void calcFactorialTest_whenLoopForValue0() {
	int factorial = LoopsPractice.calcFactorial(3, 0);
	int answer = 1;
	Assert.assertEquals(answer, factorial);
    }

    @Test
    public void calcFactorialTest_whenLoopForValue1() {
	int factorial = LoopsPractice.calcFactorial(3, 1);
	int answer = 1;
	Assert.assertEquals(answer, factorial);
    }

    @Test
    public void calcFactorialTest_whenLoopForValue5() {
	int factorial = LoopsPractice.calcFactorial(3, 5);
	int answer = 120;
	Assert.assertEquals(answer, factorial);
    }
}
