package com.klimenkov;

import java.util.Arrays;

public class Median {

    public static float median(int[] array) {
	Arrays.sort(array);
	float median;
	if (array.length % 2 == 1) {
	    median = (float) array[array.length / 2];
	} else {
	    median = (float) (array[array.length / 2] + array[array.length / 2 - 1]) / 2;
	}
	return median;
    }

    public static double median(double[] array) {
	Arrays.sort(array);
	double median;
	if (array.length % 2 == 1) {
	    median = array[array.length / 2];
	} else {
	    median = (array[array.length / 2] + array[array.length / 2 - 1]) / 2;
	}
	return median;
    }
}
