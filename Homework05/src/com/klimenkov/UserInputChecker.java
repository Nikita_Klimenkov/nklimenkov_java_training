package com.klimenkov;

public class UserInputChecker {

    public static boolean pathIsCorrect(String[] path) {
	if (!path[0].equals("root")) {
	    return false;
	}
	for (int i = 0; i < path.length - 1; i++) {
	    if (path[i].contains(".")) {
		return false;
	    }
	}
	for (int i = 0; i < path.length; i++) {
	    if (path[i].contains(",") || path[i].contains("|") || path[i].contains("\\")) {
		return false;
	    }
	}
	return true;
    }
}
