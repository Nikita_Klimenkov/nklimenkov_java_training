package com.klimenkov;

import com.klimenkov.UserInteraction;

public class Launcher {

    public static void main(String[] args) {
	Folder rootFolder = new Folder("root");
	Tree fileSystem = new Tree(rootFolder);
	UserInteraction.startDialoge(fileSystem);
    }
}
