package com.klimenkov;

import java.util.ArrayList;

public class Folder {

    private String name;
    private ArrayList<Folder> childFolders;
    private ArrayList<File> childFiles;

    public Folder(String name) {
	setName(name);
	childFolders = new ArrayList<Folder>();
	childFiles = new ArrayList<File>();
    }

    public String getName() {
	return name;
    }

    private void setName(String name) {
	if (name == null || name.isEmpty()) {
	    throw new IllegalArgumentException("Folder name can't be null.");
	} else {
	    this.name = name;
	}
    }

    public ArrayList<Folder> getChildFolders() {
	return childFolders;
    }

    public ArrayList<File> getChildFiles() {
	return childFiles;
    }

    public Folder getChildFolder(String childFolderName) {
	for (Folder tempFolder : childFolders) {
	    if (tempFolder.getName().equals(childFolderName)) {
		return tempFolder;
	    }
	}
	return null;
    }

    public boolean containsFolder(String folderName) {
	for (Folder tempFolder : childFolders) {
	    if (tempFolder.getName().equals(folderName)) {
		return true;
	    }
	}
	return false;
    }

    public boolean containsFile(File file) {
	for (File tempFile : childFiles) {
	    if (tempFile.getName().equals(file.getName()) && tempFile.getExtension().equals(file.getExtension())) {
		return true;
	    }
	}
	return false;
    }

    public void addFolder(String folderName) {
	Folder folder = new Folder(folderName);
	childFolders.add(folder);
    }

    public void addFile(File file) {
	childFiles.add(file);
    }
}
