package com.klimenkov;

import org.junit.Test;
import org.junit.Assert;
import com.klimenkov.UserInteraction;

public class UserInteractionTest {
    @Test
    public void testGetFoldersPath() {
	String[] path = { "root", "file.txt" };
	String[] foldersPath = { "root" };
	Assert.assertArrayEquals(foldersPath, UserInteraction.getFoldersPath(path));
    }

    @Test
    public void testGetFoldersPath1() {
	String[] path = { "root", "folder1", "folder2", "file.txt" };
	String[] foldersPath = { "root", "folder1", "folder2" };
	Assert.assertArrayEquals(foldersPath, UserInteraction.getFoldersPath(path));
    }
}
