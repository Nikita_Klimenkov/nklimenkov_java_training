package com.klimenkov;

import org.junit.Test;
import org.junit.Assert;
import com.klimenkov.UserInputChecker;

public class UserInputCheckerTest {
    @Test
    public void testPathIsCorrect_withTrue() {
	String userInput = "root";
	String[] path = userInput.split("/");
	Assert.assertEquals(true, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withTrue1() {
	String userInput = "root/folder1";
	String[] path = userInput.split("/");
	Assert.assertEquals(true, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withTrue2() {
	String userInput = "root/folder1/folder2/folder3";
	String[] path = userInput.split("/");
	Assert.assertEquals(true, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withTrue3() {
	String userInput = "root/file.txt";
	String[] path = userInput.split("/");
	Assert.assertEquals(true, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withTrue4() {
	String userInput = "root/folder1/file.txt";
	String[] path = userInput.split("/");
	Assert.assertEquals(true, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withFalse() {
	String userInput = "rot";
	String[] path = userInput.split("/");
	Assert.assertEquals(false, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withFalse1() {
	String userInput = "root\folder1";
	String[] path = userInput.split("/");
	Assert.assertEquals(false, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withFalse2() {
	String userInput = "root/folder1|folder2";
	String[] path = userInput.split("/");
	Assert.assertEquals(false, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withFalse3() {
	String userInput = "root/folder1/file.txt/folder2";
	String[] path = userInput.split("/");
	Assert.assertEquals(false, UserInputChecker.pathIsCorrect(path));
    }

    @Test
    public void testPathIsCorrect_withFalse4() {
	String userInput = "root/folder.txt/file.txt";
	String[] path = userInput.split("/");
	Assert.assertEquals(false, UserInputChecker.pathIsCorrect(path));
    }
}
