package com.klimenkov;

public enum AutoAtmType {
    MoneyProducer, MoneyConsumer;
}
