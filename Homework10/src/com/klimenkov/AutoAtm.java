package com.klimenkov;

public class AutoAtm implements Runnable {
    private Card card;
    private AutoAtmType atmType;
    private int balanceShift;

    public AutoAtm(Card card, AutoAtmType atmType, int balanceShift) {
	setCard(card);
	setAtmType(atmType);
	setBalanceShift(balanceShift);
    }

    public AutoAtm(Card card) {
	setCard(card);
	setAtmType(AutoAtmType.MoneyProducer);
	setBalanceShift(100);
    }

    public void setCard(Card card) {
	if (card != null) {
	    this.card = card;
	} else {
	    throw new IllegalArgumentException("Card can't be null.");
	}
    }

    public void setAtmType(AutoAtmType atmType) {
	if (atmType != null) {
	    this.atmType = atmType;
	} else {
	    throw new IllegalArgumentException("AtmType can't be null.");
	}
    }

    public void setBalanceShift(int balanceShift) {
	if (balanceShift > 0 && balanceShift < 1000) {
	    this.balanceShift = balanceShift;
	} else {
	    throw new IllegalArgumentException("BalanceShift can't be less than 0 or greater than 1000");
	}

    }

    @Override
    public void run() {
	while (true) {
	    synchronized (card) {
		if (card.getBalance() > 0 && card.getBalance() < 1000) {
		    if (atmType.equals(AutoAtmType.MoneyProducer)) {
			increaseBalance(balanceShift);
		    } else {
			decreaseBalance(balanceShift);
		    }
		} else {
		    break;
		}
	    }
	}
    }

    public void increaseBalance(int increment) {
	try {
	    Thread.sleep(1000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	System.out.println(Thread.currentThread().getName() + " going to increase balance.");
	try {
	    Thread.sleep(2000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	card.increaseBalance(increment);
	System.out.println(Thread.currentThread().getName() + " completed increasing.");
	System.out.println("Current balance: " + card.getBalance() + "$");
	System.out.println();
    }

    public void decreaseBalance(int decrement) {
	try {
	    Thread.sleep(1000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	System.out.println(Thread.currentThread().getName() + " going to decrease balance.");
	try {
	    Thread.sleep(2000);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	card.decreaseBalance(decrement);
	System.out.println(Thread.currentThread().getName() + " completed decreasing.");
	System.out.println("Current balance: " + card.getBalance() + "$");
	System.out.println();
    }
}
