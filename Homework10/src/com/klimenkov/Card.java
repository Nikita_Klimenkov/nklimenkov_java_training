package com.klimenkov;

public class Card {

    private int balance;

    public Card(int balance) {
	this.balance = balance;
    }

    public int getBalance() {
	return balance;
    }

    public void setBalance(int balance) {
	this.balance = balance;
    }

    public synchronized void increaseBalance(int increment) {
	this.balance = this.balance + increment;
    }

    public synchronized void decreaseBalance(int decrement) {
	this.balance = this.balance - decrement;
    }
}