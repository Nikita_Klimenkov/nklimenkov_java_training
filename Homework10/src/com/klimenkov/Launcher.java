package com.klimenkov;

public class Launcher {
    public static void main(String[] args) {

	Card card = new Card(500);

	AutoAtm moneyProducer1 = new AutoAtm(card, AutoAtmType.MoneyProducer, 100);
	AutoAtm moneyProducer2 = new AutoAtm(card, AutoAtmType.MoneyProducer, 100);
	AutoAtm moneyProducer3 = new AutoAtm(card, AutoAtmType.MoneyProducer, 100);
	AutoAtm moneyConsumer1 = new AutoAtm(card, AutoAtmType.MoneyConsumer, 100);
	AutoAtm moneyConsumer2 = new AutoAtm(card, AutoAtmType.MoneyConsumer, 100);
	AutoAtm moneyConsumer3 = new AutoAtm(card, AutoAtmType.MoneyConsumer, 100);

	Thread producer1 = new Thread(moneyProducer1);
	Thread producer2 = new Thread(moneyProducer2);
	Thread producer3 = new Thread(moneyProducer3);
	Thread consumer1 = new Thread(moneyConsumer1);
	Thread consumer2 = new Thread(moneyConsumer2);
	Thread consumer3 = new Thread(moneyConsumer3);

	producer1.setName("+MoneyProducer1");
	producer2.setName("+MoneyProducer2");
	producer3.setName("+MoneyProducer3");
	consumer1.setName("-MoneyConsumer1");
	consumer2.setName("-MoneyConsumer2");
	consumer3.setName("-MoneyConsumer3");

	producer1.start();
	producer2.start();
	producer3.start();
	consumer1.start();
	consumer2.start();
	consumer3.start();

	try {
	    producer1.join();
	    producer2.join();
	    producer3.join();
	    consumer1.join();
	    consumer2.join();
	    consumer3.join();
	} catch (InterruptedException e) {
	    System.out.println("Thread exception!");
	}

	System.out.println("Card balance reached limit. Balance: " + card.getBalance() + "$");
    }
}
