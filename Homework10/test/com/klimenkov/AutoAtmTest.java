package com.klimenkov;

import org.junit.Assert;
import org.junit.Test;

import com.klimenkov.AutoAtm;
import com.klimenkov.Card;

public class AutoAtmTest {
   
    @Test
    public void testIncreaseBalance0() {
	Card card = new Card(500);
	AutoAtm moneyProducer1 = new AutoAtm(card);
	moneyProducer1.increaseBalance(100);
	int expection = 600;
	Assert.assertEquals(expection, card.getBalance());
    }
    
    @Test
    public void testIncreaseBalance1() {
	Card card = new Card(700);
	AutoAtm moneyProducer1 = new AutoAtm(card);
	moneyProducer1.increaseBalance(200);
	int expection = 900;
	Assert.assertEquals(expection, card.getBalance());
    }
    
    @Test
    public void testDecreaseBalance0() {
	Card card = new Card(800);
	AutoAtm moneyConsumer1 = new AutoAtm(card);
	moneyConsumer1.decreaseBalance(100);
	int expection = 700;
	Assert.assertEquals(expection, card.getBalance());
    }
    
    @Test
    public void testDecreaseBalance1() {
	Card card = new Card(500);
	AutoAtm moneyConsumer1 = new AutoAtm(card);
	moneyConsumer1.decreaseBalance(300);
	int expection = 200;
	Assert.assertEquals(expection, card.getBalance());
    }
}
