package com.klimenkov;

import com.klimenkov.exception.ValidationFailedException;

public class StringValidator implements Validator<String> {
    public void validate(String string) throws ValidationFailedException {
	if (string == null) {
	    throw new ValidationFailedException("String can't be null.");
	}
	if (!string.matches("^[A-Z].*")) {
	    throw new ValidationFailedException("String must begin with a letter in uppercase.");
	}
    }
}