package com.klimenkov;

public class ValidationSystem {

    private static IntegerValidator integerValidator = new IntegerValidator();
    private static StringValidator stringValidator = new StringValidator();

    public static void validate(String string) {
	stringValidator.validate(string);
    }

    public static void validate(Integer integer) {
	integerValidator.validate(integer);
    }
}
