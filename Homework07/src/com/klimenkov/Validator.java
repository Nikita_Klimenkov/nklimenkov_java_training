package com.klimenkov;

import com.klimenkov.exception.ValidationFailedException;

public interface Validator<T> {
    public void validate(T input) throws ValidationFailedException;
}
