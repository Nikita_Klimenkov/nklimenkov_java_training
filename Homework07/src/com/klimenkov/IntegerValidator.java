package com.klimenkov;

import com.klimenkov.exception.ValidationFailedException;

public class IntegerValidator implements Validator<Integer> {
    public void validate(Integer integer) throws ValidationFailedException {
	if (integer == null) {
	    throw new ValidationFailedException("Integer can't be null.");
	}
	if (integer < 1 || integer > 10) {
	    throw new ValidationFailedException("Integer must be in between [1, 10].");
	}
    }
}
