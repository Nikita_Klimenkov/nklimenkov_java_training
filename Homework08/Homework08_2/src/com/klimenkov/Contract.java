package com.klimenkov;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.klimenkov.exception.PickFailedException;

public class Contract {
    private Map<Skill, Integer> demands = new HashMap<Skill, Integer>();
    private ArrayList<Team> teams = new ArrayList<Team>();

    public Contract(Map<Skill, Integer> demands, ArrayList<Team> teams) {
	this.setDemands(demands);
	this.setTeams(teams);
    }

    public Contract(Map<Skill, Integer> demands) {
	this.setDemands(demands);
    }

    public Contract() {
    }

    private void setDemands(Map<Skill, Integer> demands) {
	if (demands == null) {
	    throw new IllegalArgumentException("Demands can't be null.");
	} else {
	    this.demands = demands;
	}
    }

    private void setTeams(ArrayList<Team> teams) {
	if (teams == null) {
	    throw new IllegalArgumentException("Teams can't be null.");
	} else {
	    this.teams = teams;
	}
    }

    public Map<Skill, Integer> getDemands() {
	return this.demands;
    }

    public ArrayList<Team> teams() {
	return teams;
    }

    public void addDemand(Skill newSkill, int workersCounter) {
	if (!demands.containsKey(newSkill)) {
	    demands.put(newSkill, workersCounter);
	} else {
	    demands.put(newSkill, demands.get(newSkill) + workersCounter);
	}
    }

    public void addTeam(Team team) {
	if (team != null) {
	    teams.add(team);
	} else {
	    throw new IllegalArgumentException("Team can't be null.");
	}
    }

    public Team getCheaperTeam(ArrayList<Team> teams) {
	Team cheaperTeam = teams.get(0);
	for (int i = 0; i < teams.size(); i++) {
	    if (cheaperTeam.getPrice() > teams.get(i).getPrice()) {
		cheaperTeam = teams.get(i);
	    }
	}
	return cheaperTeam;
    }

    public Team pickTeam() throws PickFailedException{
	ArrayList<Team> suitableTeams = new ArrayList<Team>();
	for (Team team : teams) {
	    boolean isSuitableTeam = true;

	    Map<Skill, Integer> teamSkills = team.getTeamSkills();

	    for (Skill skill : demands.keySet()) {
		if (teamSkills.containsKey(skill)) {
		    if (demands.get(skill) > teamSkills.get(skill)) {
			isSuitableTeam = false;
			break;
		    }
		} else {
		    isSuitableTeam = false;
		    break;
		}
	    }
	    if (isSuitableTeam) {
		suitableTeams.add(team);
	    }
	}

	if (suitableTeams.size() == 0) {
	    throw new PickFailedException("Has no suitable team for this contract");
	}
	Team result;

	if (suitableTeams.size() == 1) {
	    result = suitableTeams.get(0);
	} else {
	    result = getCheaperTeam(suitableTeams);
	}

	return result;
    }

    @Override
    public String toString() {
	String string = "Demands: " + this.demands + ";\n" + "Teams: " + teamsToString();
	return string;
    }
    
    private String teamsToString() {
	String result = "";
	for (Team team : teams) {
	    result += "\n[" + team.toString() + "]";
	}
	return result;
    }
}
