package com.klimenkov;

import com.klimenkov.exception.PickFailedException;

public class Launcher {
    public static void main(String[] args) {
	Worker w1 = new Worker("Henry", 30);
	Worker w2 = new Worker("Henry", 40);
	Worker w3 = new Worker("Henry", 20);
	Worker w4 = new Worker("Henry", 38);
	Worker w5 = new Worker("Henry", 60);
	Worker w6 = new Worker("Henry", 13);
	
	w1.addSkill(Skill.PAINTER);
	w1.addSkill(Skill.ARCHITECTOR);
	w2.addSkill(Skill.DESIGNER);
	w3.addSkill(Skill.ENGINEER);
	w3.addSkill(Skill.TECHNICIAN);
	w4.addSkill(Skill.LOADER);
	w5.addSkill(Skill.DRIVER);
	w5.addSkill(Skill.CRANE_OPERATOR);
	w6.addSkill(Skill.WELDER);

	System.out.println("Teams:");
	Team t1 = new Team();
	t1.addWorker(w1);
	t1.addWorker(w2);
	t1.addWorker(w3);
	System.out.println(t1.toString());

	Team t2 = new Team();
	t2.addWorker(w4);
	t2.addWorker(w5);
	t2.addWorker(w6);
	System.out.println(t2.toString());

	Contract contract = new Contract();
	contract.addDemand(Skill.ECONOMIST, 1);
	contract.addDemand(Skill.ARCHITECTOR, 1);
	contract.addDemand(Skill.DESIGNER, 1);
	contract.addDemand(Skill.PAINTER, 2);

	contract.addTeam(t1);
	contract.addTeam(t2);
	System.out.println("\nContract Information: ");
	System.out.println(contract.toString());

	try {
	    System.out.println("PickedTeam: " + contract.pickTeam().toString());
	} catch (PickFailedException e) {
	    System.out.println(e.getMessage());
	}
    }
}
