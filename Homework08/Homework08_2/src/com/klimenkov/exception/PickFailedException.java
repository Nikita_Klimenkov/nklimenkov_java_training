package com.klimenkov.exception;

public class PickFailedException extends Exception {
    public PickFailedException(String message) {
	super(message);
    }
}
