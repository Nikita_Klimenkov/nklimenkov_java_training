package com.klimenkov;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Team {
    private ArrayList<Worker> workers = new ArrayList<Worker>();

    public Team(ArrayList<Worker> workers) {
	this.setWorker(workers);
    }

    public Team() {
    }

    private void setWorker(ArrayList<Worker> workers) {
	if (workers == null) {
	    throw new IllegalArgumentException("Workers can't be null.");
	} else {
	    this.workers = workers;
	}
    }

    public double getPrice() {
	double teamPrice = 0;
	for (Worker worker : workers) {
	    teamPrice += worker.getPrice();
	}
	return teamPrice;
    }

    public void addWorker(Worker worker) {
	if (worker != null) {
	    workers.add(worker);
	} else {
	    throw new IllegalArgumentException("Worker can't be null.");
	}
    }

    public Map<Skill, Integer> getTeamSkills() {
	Map<Skill, Integer> team�apabilities = new HashMap<Skill, Integer>();
	int workersWithSkillCounter = 0;
	ArrayList<Skill> skills = getSkillsList();

	for (Skill skill : Skill.values()) {
	    workersWithSkillCounter = 0;
	    for (Skill workerSkill : skills) {
		if (skill.equals(workerSkill)) {
		    workersWithSkillCounter++;
		}
	    }
	    if (workersWithSkillCounter != 0) {
		team�apabilities.put(skill, workersWithSkillCounter);
	    }
	}
	return team�apabilities;
    }

    @Override
    public String toString() {
	String string = "Price: " + getPrice() + "; TeamSkills: " + getTeamSkills().toString();
	return string;
    }
    
    private ArrayList<Skill> getSkillsList() {
	ArrayList<Skill> teamSkills = new ArrayList<Skill>();
	for (Worker worker : this.workers) {
	    teamSkills.addAll(worker.getSkills());
	}
	return teamSkills;
    }
}
