package com.klimenkov;

import java.util.Set;
import java.util.TreeSet;

public class Worker {
    private String name;
    private double price;
    private Set<Skill> skills = new TreeSet<Skill>();

    public Worker(String name, double price, Set<Skill> skills) {
	this.setName(name);
	this.setPirce(price);
	this.setSkills(skills);
    }

    public Worker(String name, double price) {
	this.setName(name);
	this.setPirce(price);
    }

    public String getName() {
	return this.name;
    }

    public double getPrice() {
	return this.price;
    }

    public Set<Skill> getSkills() {
	return this.skills;
    }

    private void setName(String name) {
	if (name == null || name.isEmpty()) {
	    throw new IllegalArgumentException("Name can't be null or empty.");
	} else {
	    this.name = name;
	}
    }

    private void setPirce(double price) {
	if (price <= 0) {
	    throw new IllegalArgumentException("Price can't be 0 or negative.");
	} else {
	    this.price = price;
	}
    }

    private void setSkills(Set<Skill> skills) {
	if (skills == null) {
	    throw new IllegalArgumentException("Skills can't be null.");
	} else {
	    this.skills = skills;
	}
    }

    public void addSkill(Skill skill) {
	if (skill != null) {
	    skills.add(skill);
	} else {
	    throw new IllegalArgumentException("Skill can't be null.");
	}
    }

    @Override
    public String toString() {
	String string = "Name: " + this.name.toString() + "; Price: " + this.price + "; Skills:" + this.getSkills();
	return string;
    }
}
