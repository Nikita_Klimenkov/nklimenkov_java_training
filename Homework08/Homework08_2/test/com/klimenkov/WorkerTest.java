package com.klimenkov;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

import com.klimenkov.Skill;
import com.klimenkov.Worker;

public class WorkerTest {
    @Test(expected = IllegalArgumentException.class)
    public void testAddSkill_Exception() {
	Worker worker = new Worker("Henry", 15);
	worker.addSkill(null);
    }
    
    @Test
    public void testAddSkill() {
	Set<Skill> skills = new TreeSet<Skill>();
	skills.add(Skill.TECHNOLOGIST);
	skills.add(Skill.ARCHITECTOR);
	Worker workerExpected = new Worker("Henry", 15, skills);

	Worker workerActual = new Worker("Henry", 15);
	workerActual.addSkill(Skill.TECHNOLOGIST);
	workerActual.addSkill(Skill.ARCHITECTOR);

	Assert.assertEquals(workerExpected.toString(), workerActual.toString());
    }
}
