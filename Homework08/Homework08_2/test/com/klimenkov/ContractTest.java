package com.klimenkov;

import org.junit.Assert;
import org.junit.Test;

import com.klimenkov.Contract;
import com.klimenkov.Skill;
import com.klimenkov.Team;
import com.klimenkov.Worker;
import com.klimenkov.exception.PickFailedException;

public class ContractTest {

    @Test
    public void testPickUpTeam() throws PickFailedException{
	Worker w1 = new Worker("Henry", 15);
	Worker w2 = new Worker("Henry", 16);
	Worker w3 = new Worker("Henry", 26);
	Worker w4 = new Worker("Henry", 50);
	Worker w5 = new Worker("Henry", 30);
	Worker w6 = new Worker("Henry", 30);
	
	w1.addSkill(Skill.ECONOMIST);
	w1.addSkill(Skill.PAINTER);
	w1.addSkill(Skill.ARCHITECTOR);
	w2.addSkill(Skill.DESIGNER);
	w2.addSkill(Skill.PAINTER);
	w3.addSkill(Skill.ENGINEER);
	
	w4.addSkill(Skill.LOADER);
	w5.addSkill(Skill.DRIVER);
	w5.addSkill(Skill.CRANE_OPERATOR);
	w6.addSkill(Skill.WELDER);
	
	Team t1 = new Team();
	t1.addWorker(w1);
	t1.addWorker(w2);
	t1.addWorker(w3);

	Team t2 = new Team();
	t2.addWorker(w4);
	t2.addWorker(w5);
	t2.addWorker(w6);
	

	Contract contract = new Contract();
	contract.addDemand(Skill.ECONOMIST, 1);
	contract.addDemand(Skill.ARCHITECTOR, 1);
	contract.addDemand(Skill.DESIGNER, 1);
	contract.addDemand(Skill.PAINTER, 2);

	contract.addTeam(t1);
	contract.addTeam(t2);

	Assert.assertEquals(t1.toString(), contract.pickTeam().toString());
    }

    @Test
    public void testPickUpTeam_SameSkils_DifferentPrice() throws PickFailedException{
	Worker w1 = new Worker("Henry", 1000);
	Worker w2 = new Worker("Henry", 20);
	Worker w3 = new Worker("Henry", 26);
	Worker w4 = new Worker("Henry", 50);
	Worker w5 = new Worker("Henry", 30);
	Worker w6 = new Worker("Henry", 30);
	
	w1.addSkill(Skill.ARCHITECTOR);
	w2.addSkill(Skill.DESIGNER);
	w3.addSkill(Skill.ECONOMIST);
	
	w4.addSkill(Skill.ARCHITECTOR);
	w5.addSkill(Skill.DESIGNER);
	w6.addSkill(Skill.ECONOMIST);
	
	Team t1 = new Team();
	t1.addWorker(w1);
	t1.addWorker(w2);
	t1.addWorker(w3);

	Team t2 = new Team();
	t2.addWorker(w4);
	t2.addWorker(w5);
	t2.addWorker(w6);

	Contract contract = new Contract();
	contract.addDemand(Skill.ECONOMIST, 1);
	contract.addDemand(Skill.ARCHITECTOR, 1);
	contract.addDemand(Skill.DESIGNER, 1);

	contract.addTeam(t1);
	contract.addTeam(t2);

	Assert.assertEquals(t2.toString(), contract.pickTeam().toString());
    }

    @Test(expected = PickFailedException.class)
    public void testPickUpTeam_Exception() throws PickFailedException {
	Worker w1 = new Worker("Henry", 41);
	Worker w2 = new Worker("Henry", 20);
	Worker w3 = new Worker("Henry", 26);
	Worker w4 = new Worker("Henry", 50);
	Worker w5 = new Worker("Henry", 30);
	Worker w6 = new Worker("Henry", 30);

	w1.addSkill(Skill.ARCHITECTOR);
	w2.addSkill(Skill.DESIGNER);
	w3.addSkill(Skill.ECONOMIST);
	
	w4.addSkill(Skill.ARCHITECTOR);
	w5.addSkill(Skill.DESIGNER);
	w6.addSkill(Skill.DESIGNER);

	Team t1 = new Team();
	t1.addWorker(w1);
	t1.addWorker(w2);
	t1.addWorker(w3);
	

	Team t2 = new Team();
	t2.addWorker(w4);
	t2.addWorker(w5);
	t2.addWorker(w6);

	Contract contract = new Contract();

	contract.addDemand(Skill.ECONOMIST, 1);
	contract.addDemand(Skill.ARCHITECTOR, 1);
	contract.addDemand(Skill.DESIGNER, 3);

	contract.addTeam(t1);
	contract.addTeam(t2);

	contract.pickTeam();
    }
}
