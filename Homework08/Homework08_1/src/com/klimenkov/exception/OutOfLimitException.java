package com.klimenkov.exception;

public class OutOfLimitException extends RuntimeException {
    public OutOfLimitException(String message) {
	super(message);
    }
}
