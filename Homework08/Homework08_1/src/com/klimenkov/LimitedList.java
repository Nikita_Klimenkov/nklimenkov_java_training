package com.klimenkov;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.klimenkov.exception.OutOfLimitException;

public class LimitedList<T> extends ArrayList<T> implements List<T> {

    private int limit;

    public LimitedList(int limit) {
	this.limit = limit;
    }

    private void limitCheckForAdd() {
	if (size() == limit)
	    throw new OutOfLimitException("List is filled.");
    }

    private void limitCheckForAddAll(int itemsSize) {
	if (itemsSize + size() > limit)
	    throw new OutOfLimitException("List is filled.");
    }

    @Override
    public boolean add(T item) {
	limitCheckForAdd();
	return super.add(item);
    }

    @Override
    public void add(int index, T item) {
	limitCheckForAdd();
	super.add(index, item);
    }

    @Override
    public boolean addAll(Collection<? extends T> items) {
	limitCheckForAddAll(items.size());
	return super.addAll(items);
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> items) {
	limitCheckForAddAll(items.size());
	return super.addAll(index, items);
    }
}
