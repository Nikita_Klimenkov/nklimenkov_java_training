package com.klimenkov;

import org.junit.Test;
import org.junit.Assert;
import java.util.ArrayList;

public class WordSorterTest {
    @Test
    public void testGetSortedWordList1() {
	String text = "Apple.";
	ArrayList<WordsByLetterList> sortedWordList = WordSorter.getSortedWordList(text);
	WordsByLetterList listA = sortedWordList.get(0);
	ArrayList<String> wordsActual = listA.getWordsByLetterList();

	ArrayList<String> wordsExpected = new ArrayList<String>();
	wordsExpected.add("apple");

	Assert.assertEquals(wordsExpected, wordsActual);
    }

    @Test
    public void testGetSortedWordList2() {
	String text = "BALL; bee, Bus!";
	ArrayList<WordsByLetterList> sortedWordList = WordSorter.getSortedWordList(text);
	WordsByLetterList listB = sortedWordList.get(0);
	ArrayList<String> wordsActual = listB.getWordsByLetterList();

	ArrayList<String> wordsExpected = new ArrayList<String>();
	wordsExpected.add("ball");
	wordsExpected.add("bee");
	wordsExpected.add("bus");

	Assert.assertEquals(wordsExpected, wordsActual);
    }

    @Test
    public void testGetSortedWordList3() {
	String text = "chicken + CAT";
	ArrayList<WordsByLetterList> sortedWordList = WordSorter.getSortedWordList(text);
	WordsByLetterList listC = sortedWordList.get(0);
	ArrayList<String> wordsActual = listC.getWordsByLetterList();

	ArrayList<String> wordsExpected = new ArrayList<String>();
	wordsExpected.add("cat");
	wordsExpected.add("chicken");

	Assert.assertEquals(wordsExpected, wordsActual);
    }

    @Test
    public void testGetSortedWordList4() {
	String text = "duck... DUCK!";
	ArrayList<WordsByLetterList> sortedWordList = WordSorter.getSortedWordList(text);
	WordsByLetterList listD = sortedWordList.get(0);
	ArrayList<String> wordsActual = listD.getWordsByLetterList();

	ArrayList<String> wordsExpected = new ArrayList<String>();
	wordsExpected.add("duck");

	Assert.assertEquals(wordsExpected, wordsActual);
    }
}
