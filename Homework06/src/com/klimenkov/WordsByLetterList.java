package com.klimenkov;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Objects of this class store list of words beginning with the same letter.
 */
public class WordsByLetterList {

    private char firstLetter;

    private ArrayList<String> wordsByLetterList;

    public WordsByLetterList(char firstLetter) {
	setFirstLetter(firstLetter);
	wordsByLetterList = new ArrayList<String>();
    }

    public char getFirstLetter() {
	return firstLetter;
    }

    public void setFirstLetter(char firstLetter) {
	this.firstLetter = firstLetter;
    }

    public ArrayList<String> getWordsByLetterList() {
	return wordsByLetterList;
    }

    public void setWordsByLetterList(ArrayList<String> wordsByLetterList) {
	if (wordsByLetterList == null) {
	    throw new IllegalArgumentException("Word list can't be null.");
	} else {
	    this.wordsByLetterList = wordsByLetterList;
	}
    }

    public void addWord(String word) {
	if (word == null || word.isEmpty()) {
	    throw new IllegalArgumentException("Word can't be null or empty.");
	} else {
	    wordsByLetterList.add(word);
	}
    }

    public void sort() {
	Collections.sort(wordsByLetterList);
    }
}
