package com.klimenkov;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This class contains methods that sort the words grouping them by the first
 * letters in alphabetical order without duplicates. Words beginning with the
 * same letter are also sorted in alphabetical order. Sorting is not case
 * sensitive.
 */
public class WordSorter {

    public static void sortWordList(ArrayList<Character> letterList, ArrayList<WordsByLetterList> wordList) {
	for (int i = letterList.size() - 1; i > 0; i--) {
	    for (int j = 0; j < i; j++) {
		if (letterList.get(j) > letterList.get(j + 1)) {
		    Collections.swap(letterList, j, j + 1);
		    Collections.swap(wordList, j, j + 1);
		}
	    }
	}
	for (WordsByLetterList wordsByLetterList : wordList) {
	    wordsByLetterList.sort();
	}
    }

    public static ArrayList<WordsByLetterList> getSortedWordList(String text) {
	String[] words = text.split("[^A-Za-z]+");
	ArrayList<WordsByLetterList> wordList = new ArrayList<WordsByLetterList>();
	ArrayList<Character> letterList = new ArrayList<Character>();
	Character wordFirstLetter;
	for (String word : words) {
	    word = word.toLowerCase();
	    wordFirstLetter = new Character(Character.toUpperCase(word.charAt(0)));
	    if (letterList.contains(wordFirstLetter)) {
		int i = letterList.indexOf(wordFirstLetter);
		ArrayList<String> newList = wordList.get(i).getWordsByLetterList();
		if (!newList.contains(word)) {
		    newList.add(word);
		    wordList.get(i).setWordsByLetterList(newList);
		}
	    } else {
		letterList.add(wordFirstLetter);
		WordsByLetterList newList = new WordsByLetterList(wordFirstLetter);
		newList.addWord(word);
		wordList.add(newList);
	    }
	}
	sortWordList(letterList, wordList);
	return wordList;
    }
}
