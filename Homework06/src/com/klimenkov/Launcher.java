package com.klimenkov;

import com.klimenkov.WordSorter;
import com.klimenkov.WordListPrinter;
import java.util.ArrayList;
import java.util.Scanner;

public class Launcher {

    public static void main(String[] args) {
	System.out.println("Enter the text.");
	Scanner in = new Scanner(System.in);
	String userInput = in.nextLine();
	ArrayList<WordsByLetterList> sortedWordList = WordSorter.getSortedWordList(userInput);
	WordListPrinter.printWordList(sortedWordList);
	in.close();
    }
}
