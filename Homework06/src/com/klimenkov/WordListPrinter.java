package com.klimenkov;

import java.util.ArrayList;

public class WordListPrinter {

    public static void printWordList(ArrayList<WordsByLetterList> wordList) {
	for (WordsByLetterList tempList : wordList) {
	    System.out.print(tempList.getFirstLetter() + ": ");
	    ArrayList<String> wordsByLetterList = tempList.getWordsByLetterList();
	    for (String word : wordsByLetterList) {
		System.out.print(word + " ");
	    }
	    System.out.println();
	}
    }
}
