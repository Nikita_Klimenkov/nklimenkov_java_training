package com.klimenkov;

public class Sorter {
    public static void main(String[] args) {
	int[] array = { 1004, 300, 202 };
	sortByDigitsSum(array);
	printArray(array);
    }

    public static void sortByDigitsSum(int[] array) {
	for (int i = 0; i < array.length - 1; i++) {
	    for (int j = i + 1; j < array.length; j++) {
		if (getDigitsSum(array[i]) > getDigitsSum(array[j])) {
		    swapNumbers(i, j, array);
		}
	    }
	}
    }

    public static int getDigitsSum(int num) {
	int sum = 0;
	while (num > 0) {
	    sum = sum + num % 10;
	    num = num / 10;
	}
	return sum;
    }

    private static void printArray(int[] array) {
	for (int i : array) {
	    System.out.println(i);
	}
    }

    private static void swapNumbers(int i, int j, int[] array) {
	int temp;
	temp = array[i];
	array[i] = array[j];
	array[j] = temp;
    }
}
