package com.klimenkov;

import org.junit.Assert;
import org.junit.Test;

import com.klimenkov.Sorter;

public class SorterTest {

    @Test
    public void testSortByDigitsSum0() {
	int[] array = { 106, 204, 109, 1004 };
	Sorter.sortByDigitsSum(array);

	int[] expected = { 1004, 204, 106, 109 };

	Assert.assertArrayEquals(expected, array);
    }

    @Test
    public void testSortByDigitsSum1() {
	int[] array = { 1, 0, 301, 104 };
	Sorter.sortByDigitsSum(array);

	int[] expected = { 0, 1, 301, 104 };

	Assert.assertArrayEquals(expected, array);
    }
    
    @Test
    public void testGetDigitsSum0() {
	Assert.assertEquals(16, Sorter.getDigitsSum(88));
    }

    @Test
    public void testGetDigitsSum1() {
	Assert.assertEquals(5, Sorter.getDigitsSum(32));
    }
}
