package com.klimenkov;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

import com.klimenkov.SetOperations;

public class SetOperationsTest {

    HashSet<Integer> set1 = new HashSet<Integer>();
    HashSet<Integer> set2 = new HashSet<Integer>();
    {
	set1.add(4);
	set1.add(6);
	set1.add(25);
	set1.add(67);
	set2.add(4);
	set2.add(6);
	set2.add(10);
	set2.add(28);
    }

    @Test
    public void testGetUnion() {
	HashSet<Integer> expected = new HashSet<Integer>();
	expected.add(25);
	expected.add(67);
	expected.add(4);
	expected.add(6);
	expected.add(10);
	expected.add(28);
	expected.add(4);
	expected.add(6);
	HashSet<Integer> actual = SetOperations.getUnion(set1, set2);
	Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetIntersection() {
	HashSet<Integer> expected = new HashSet<Integer>();
	expected.add(4);
	expected.add(6);
	HashSet<Integer> actual = SetOperations.getIntersection(set1, set2);
	Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetSubtraction() {
	HashSet<Integer> expected = new HashSet<Integer>();
	expected.add(67);
	expected.add(25);
	HashSet<Integer> actual = SetOperations.getSubtraction(set1, set2);
	Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetExclusion() {
	HashSet<Integer> expected = new HashSet<Integer>();
	expected.add(67);
	expected.add(25);
	expected.add(28);
	expected.add(10);
	HashSet<Integer> actual = SetOperations.getExclusion(set1, set2);
	Assert.assertEquals(expected, actual);
    }
}
