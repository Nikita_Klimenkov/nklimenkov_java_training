package org.training.nklimenkov.exception;

public class ExistingComponentException extends Exception {
    public ExistingComponentException(String message) {
	super(message);
    }
}
