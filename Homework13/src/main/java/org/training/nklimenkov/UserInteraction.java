package org.training.nklimenkov;

import org.training.nklimenkov.TreeBuilder;
import org.training.nklimenkov.TreePrinter;
import org.training.nklimenkov.UserInputChecker;
import org.training.nklimenkov.exception.ExistingComponentException;

import java.io.IOException;
import java.util.Scanner;

public class UserInteraction {

    public static void printInfo() {
	System.out.println("This is an imitation of a file system.");
	System.out.println("Root directory name is 'root'.");
	System.out.println("Files must have name and extension separated by a dot.");
	System.out.println("Available commands(enter without quotes):");
	System.out.println("'root/folder1/file.txt' - adds the introduced folders and files to the file system;");
	System.out.println("'print' - displays the file system structure;");
	System.out.println("'save' - save current tree.");
	System.out.println("'load' - load saved tree.");
	System.out.println("'exit' - exits the program.");
    }

    public static File getFile(String[] path) {
	String[] fileArr = path[path.length - 1].split("\\.");
	File file = new File(fileArr[0], fileArr[1]);
	return file;
    }

    public static String[] getFoldersPath(String[] path) {
	String[] foldersPath = new String[path.length - 1];
	for (int i = 0; i < foldersPath.length; i++) {
	    foldersPath[i] = path[i];
	}
	return foldersPath;
    }

    public static void startDialoge(Tree tree) {
	printInfo();
	TreeSerializer treeSerializer = new TreeSerializer("TreeData.ser");
	Scanner in = new Scanner(System.in);
	String userInput = in.nextLine();
	while (!userInput.equals("exit")) {
	    // serialization
	    if (userInput.equals("save")) {
		try {
		    treeSerializer.saveTree(tree);
		    System.out.println("Save completed!");
		} catch (IOException e1) {
		    System.out.println("Error. Save failed!!!");
		}
	    } else if (userInput.equals("load")) {
		try {
		    tree = treeSerializer.loadTree();
		    System.out.println("Load completed!");
		} catch (ClassNotFoundException | IOException e) {
		    System.out.println("Error. Load failed!!!");
		}
	    } else {
		// END of serialization
		if (userInput.equals("print")) {
		    TreePrinter.printFolder(tree.getRootFolder(), 0);
		} else {
		    String[] path = userInput.split("/");
		    if (UserInputChecker.pathIsCorrect(path)) {
			if (path[path.length - 1].contains(".")) {
			    File file = getFile(path);
			    String[] foldersPath = getFoldersPath(path);
			    try {
				TreeBuilder.buildPath(tree, foldersPath, file);
			    } catch (ExistingComponentException ex) {
				System.out.println(ex.getMessage());
			    }
			} else {
			    try {
				TreeBuilder.buildPath(tree, path, null);
			    } catch (ExistingComponentException ex) {
				System.out.println(ex.getMessage());
			    }
			}
		    } else {
			System.out.println("Your input is incorrect. Try again.");
		    }
		}
	    }
	    userInput = in.nextLine();
	}
	in.close();
    }
}
