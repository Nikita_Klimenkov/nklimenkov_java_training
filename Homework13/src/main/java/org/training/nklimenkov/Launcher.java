package org.training.nklimenkov;

import org.training.nklimenkov.UserInteraction;

public class Launcher {
    public static void main(String[] args) {
	Folder rootFolder = new Folder("root");
	Tree fileSystem = new Tree(rootFolder);
	UserInteraction.startDialoge(fileSystem);
    }
}
