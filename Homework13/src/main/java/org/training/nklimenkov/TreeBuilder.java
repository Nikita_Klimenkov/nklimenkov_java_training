package org.training.nklimenkov;

import org.training.nklimenkov.exception.ExistingComponentException;

public class TreeBuilder {

    public static void buildPath(Tree tree, String[] folders, File file) throws ExistingComponentException {
	Folder tempFolder = tree.getRootFolder();
	int i = 1;
	while (i < folders.length && tempFolder.containsFolder(folders[i])) {
	    tempFolder = tempFolder.getChildFolder(folders[i]);
	    i++;
	}
	
	if (i == folders.length && file == null) {
	    throw new ExistingComponentException("Folder with the same name already exists in this directory.");
	} else {
	    while (i < folders.length) {
		tempFolder.addFolder(folders[i]);
		tempFolder = tempFolder.getChildFolder(folders[i]);
		i++;
	    }
	}
	
	if (file != null) {
	    if (tempFolder.containsFile(file)) {
		throw new ExistingComponentException("File with the same name and extension "
			+ "already exists in this directory.");
	    } else {
		tempFolder.addFile(file);
	    }
	}
    }
}
