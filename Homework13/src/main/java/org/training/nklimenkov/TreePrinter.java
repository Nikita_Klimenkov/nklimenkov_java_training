package org.training.nklimenkov;

public class TreePrinter {

    public static void printIndent(int indent) {
	for (int i = 0; i < indent; i++) {
	    System.out.print("    ");
	}
    }

    public static void printFolder(Folder folder, int indent) {
	printIndent(indent);
	System.out.println(folder.getName() + "/");
	indent++;
	for (Folder tempFolder : folder.getChildFolders()) {
	    printFolder(tempFolder, indent);
	}
	for (File tempFile : folder.getChildFiles()) {
	    printFile(tempFile, indent);
	}
    }

    public static void printFile(File file, int indent) {
	printIndent(indent);
	System.out.println(file.getName() + "." + file.getExtension());
    }
}
