package org.training.nklimenkov;

import org.junit.Test;
import org.junit.Assert;

public class FolderTest {
    @Test
    public void testContainsFolder_withTrue() {
	Folder folder = new Folder("root");
	String childFolderName = "abc";
	folder.addFolder(childFolderName);
	Assert.assertEquals(true, folder.containsFolder(childFolderName));
    }

    @Test
    public void testContainsFolder_withFalse() {
	Folder folder = new Folder("root");
	String childFolderName = "abc";
	folder.addFolder(childFolderName);
	Assert.assertEquals(false, folder.containsFolder(childFolderName + "d"));
    }

    @Test
    public void testContainsFile_withTrue() {
	Folder folder = new Folder("root");
	File file = new File("file", "txt");
	folder.addFile(file);
	Assert.assertEquals(true, folder.containsFile(file));
    }

    @Test
    public void testContainsFile_withFalse() {
	Folder folder = new Folder("root");
	File file = new File("file", "txt");
	folder.addFile(file);
	File newFile = new File("file", "java");
	Assert.assertEquals(false, folder.containsFile(newFile));
    }
}
