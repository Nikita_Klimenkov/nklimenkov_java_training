package com.klimenkov;

import java.io.Serializable;

public class Tree implements Serializable{

    private Folder rootFolder;

    public Tree(Folder rootFolder) {
	setRootFolder(rootFolder);
    }

    public Folder getRootFolder() {
	return rootFolder;
    }

    public void setRootFolder(Folder rootFolder) {
	if (rootFolder == null) {
	    throw new NullPointerException("Root folder can't be null.");
	} else {
	    this.rootFolder = rootFolder;
	}
    }
}
