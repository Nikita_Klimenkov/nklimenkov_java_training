package com.klimenkov;

import java.io.Serializable;

public class File implements Serializable{

    private String name;
    private String extension;

    public File(String name, String extension) {
	setName(name);
	setExtension(extension);
    }

    public String getName() {
	return name;
    }

    public String getExtension() {
	return extension;
    }

    private void setName(String name) {
	if (name == null || name.isEmpty()) {
	    throw new IllegalArgumentException("File name can't be null or empty.");
	} else {
	    this.name = name;
	}
    }

    private void setExtension(String extension) {
	if (extension == null || extension.isEmpty()) {
	    throw new IllegalArgumentException("File extension can't be null or empty.");
	} else {
	    this.extension = extension;
	}
    }
}
