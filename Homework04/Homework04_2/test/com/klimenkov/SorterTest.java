package com.klimenkov;

import org.junit.Test;
import org.junit.Assert;

public class SorterTest {
    @Test
    public void testBubbleSort0() {
	int[] array = { 0 };
	Sorter sortStrategy = new BubbleSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { 0 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testBubbleSort1() {
	int[] array = { 1, 2, 3 };
	Sorter sortStrategy = new BubbleSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { 1, 2, 3 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testBubbleSort2() {
	int[] array = { 3, 2, 1 };
	Sorter sortStrategy = new BubbleSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { 1, 2, 3 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testBubbleSort3() {
	int[] array = { 1, 2, 1 };
	Sorter sortStrategy = new BubbleSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { 1, 1, 2 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testBubbleSort4() {
	int[] array = { 42, 3, 88, 40, 2017, -42, 0 };
	Sorter sortStrategy = new BubbleSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { -42, 0, 3, 40, 42, 88, 2017 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testSelectionSort0() {
	int[] array = { 0 };
	Sorter sortStrategy = new SelectionSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { 0 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testSelectionSort1() {
	int[] array = { 1, 2, 3 };
	Sorter sortStrategy = new SelectionSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { 1, 2, 3 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testSelectionSort2() {
	int[] array = { 3, 2, 1 };
	Sorter sortStrategy = new SelectionSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { 1, 2, 3 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testSelectionSort3() {
	int[] array = { 1, 2, 1 };
	Sorter sortStrategy = new SelectionSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { 1, 1, 2 };
	Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testSelectionSort4() {
	int[] array = { 42, 3, 88, 40, 2017, -42, 0 };
	Sorter sortStrategy = new SelectionSort();
	SortingContext context = new SortingContext(sortStrategy);
	context.execute(array);
	int[] expectedArray = { -42, 0, 3, 40, 42, 88, 2017 };
	Assert.assertArrayEquals(expectedArray, array);
    }
}
