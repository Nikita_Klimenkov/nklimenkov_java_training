package com.klimenkov;

public class SelectionSort extends Sorter {

    @Override
    protected void sort(int[] array) {
	for (int i = 0; i < array.length - 1; i++) {
	    int minValueIndex = i;
	    for (int j = i + 1; j < array.length; j++) {
		if (array[j] < array[minValueIndex]) {
		    minValueIndex = j;
		}
	    }
	    swap(i, minValueIndex, array);
	}
    }
}
