package com.klimenkov;

import java.math.BigDecimal;
import java.math.RoundingMode;
import com.klimenkov.exeptions.NegativeBalanceException;

/**
 * DebitCard is a Card that won't allow its owner to have a negative balance.
 * All withdrawal operations that will lead to a negative balance will be rejected.
 */
public class DebitCard extends Card {

    public DebitCard(String owner) {
	super(owner);
    }

    public DebitCard(String owner, BigDecimal balance) {
	super(owner, balance);
    }

    public DebitCard(String owner, double balance) {
	super(owner, balance);
    }

    public DebitCard(BigDecimal balance, String owner) {
	super(balance, owner);
    }

    public DebitCard(double balance, String owner) {
	super(balance, owner);
    }

    @Override
    public void decreaseBalance(BigDecimal decrement) {
	if (decrement == null) {
	    throw new IllegalArgumentException("Decrement can't be null.");
	} else {
	    if (balance.compareTo(decrement) == -1) {
		throw new NegativeBalanceException(
			"The withdrawal operation was rejected - there is not enough balance.");
	    } else {
		balance = balance.subtract(decrement).setScale(2, RoundingMode.HALF_UP);
	    }
	}
    }

    @Override
    public void decreaseBalance(double decrement) {
	if (balance.compareTo(new BigDecimal(decrement).setScale(2, RoundingMode.HALF_UP)) == -1) {
	    throw new NegativeBalanceException("The withdrawal operation was rejected - there is not enough balance.");
	} else {
	    balance = balance.subtract(new BigDecimal(decrement)).setScale(2, RoundingMode.HALF_UP);
	}
    }
}
