package com.klimenkov.exeptions;

public class OutOfDebtLimitException extends RuntimeException {
    public OutOfDebtLimitException(String message) {
	super(message);
    }
}
