package com.klimenkov.exeptions;

public class InsertedCardException extends RuntimeException {
    public InsertedCardException(String message) {
	super(message);
    }
}
