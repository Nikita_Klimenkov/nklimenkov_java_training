package com.klimenkov.exeptions;

public class AbsentCardException extends RuntimeException {
    public AbsentCardException(String message) {
	super(message);
    }
}
