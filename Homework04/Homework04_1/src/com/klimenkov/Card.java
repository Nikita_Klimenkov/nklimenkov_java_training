package com.klimenkov;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Card {

    protected String owner;

    protected BigDecimal balance;

    public Card(String owner) {
	if (owner == null || owner.isEmpty()) {
	    throw new NullPointerException("Name of card owner can't be null or empty.");
	} else {
	    this.owner = owner;
	    this.balance = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
	}
    }

    public Card(String owner, BigDecimal balance) {
	if (owner == null || owner.isEmpty()) {
	    throw new NullPointerException("Name of card owner can't be null or empty.");
	} else {
	    this.owner = owner;
	    this.balance = balance.setScale(2, RoundingMode.HALF_UP);
	}
    }

    public Card(String owner, double balance) {
	if (owner == null || owner.isEmpty()) {
	    throw new NullPointerException("Name of card owner can't be null or empty.");
	} else {
	    this.owner = owner;
	    this.balance = new BigDecimal(balance).setScale(2, RoundingMode.HALF_UP);
	}
    }

    public Card(BigDecimal balance, String owner) {
	if (owner == null || owner.isEmpty()) {
	    throw new NullPointerException("Name of card owner can't be null or empty.");
	} else {
	    this.owner = owner;
	    this.balance = balance.setScale(2, RoundingMode.HALF_UP);
	}
    }

    public Card(double balance, String owner) {
	if (owner == null || owner.isEmpty()) {
	    throw new NullPointerException("Name of card owner can't be null or empty.");
	} else {
	    this.owner = owner;
	    this.balance = new BigDecimal(balance).setScale(2, RoundingMode.HALF_UP);
	}
    }

    public String getOwner() {
	return owner;
    }

    public BigDecimal getBalance() {
	return balance;
    }

    public void increaseBalance(BigDecimal increment) {
	balance = balance.add(increment).setScale(2, RoundingMode.HALF_UP);
    }

    public void increaseBalance(double increment) {
	balance = balance.add(new BigDecimal(increment)).setScale(2, RoundingMode.HALF_UP);
    }

    public void decreaseBalance(BigDecimal decrement) {
	balance = balance.subtract(decrement).setScale(2, RoundingMode.HALF_UP);
    }

    public void decreaseBalance(double decrement) {
	balance = balance.subtract(new BigDecimal(decrement)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getConvertedBalance(BigDecimal conversionRate) {
	return balance.multiply(conversionRate).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getConvertedBalance(double conversionRate) {
	BigDecimal rate = new BigDecimal(conversionRate);
	return balance.multiply(rate).setScale(2, RoundingMode.HALF_UP);
    }
}
