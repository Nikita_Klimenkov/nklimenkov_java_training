package com.klimenkov;

import java.math.BigDecimal;
import java.math.RoundingMode;
import com.klimenkov.exeptions.OutOfDebtLimitException;

/**
 * CreditCard is a Card that allows its owner to withdraw money 
 * even when the balance is negative, but until a certain limit - debt limit, 
 * which must be specified while creating a new object of CreditCard type.
 * Also while creating a new object of CreditCard type you can pass 
 * to the class field 'debtLimit' both positive and negative values, 
 * they will be converted and stored as a positive BigDecimal value.
 */
public class CreditCard extends Card {

    private BigDecimal debtLimit;

    public CreditCard(String owner, BigDecimal debtLimit) {
	super(owner);
	if (debtLimit == null) {
	    throw new IllegalArgumentException("Debt limit can't be null.");
	} else {
	    this.debtLimit = debtLimit.setScale(2, RoundingMode.HALF_UP).abs();
	}
    }

    public CreditCard(String owner, BigDecimal balance, BigDecimal debtLimit) {
	super(owner, balance);
	if (debtLimit == null) {
	    throw new IllegalArgumentException("Debt limit can't be null.");
	} else {
	    this.debtLimit = debtLimit.setScale(2, RoundingMode.HALF_UP).abs();
	}
    }

    public CreditCard(String owner, double balance, BigDecimal debtLimit) {
	super(owner, balance);
	if (debtLimit == null) {
	    throw new IllegalArgumentException("Debt limit can't be null.");
	} else {
	    this.debtLimit = debtLimit.setScale(2, RoundingMode.HALF_UP).abs();
	}
    }

    public CreditCard(BigDecimal balance, String owner, BigDecimal debtLimit) {
	super(balance, owner);
	if (debtLimit == null) {
	    throw new IllegalArgumentException("Debt limit can't be null.");
	} else {
	    this.debtLimit = debtLimit.setScale(2, RoundingMode.HALF_UP).abs();
	}
    }

    public CreditCard(double balance, String owner, BigDecimal debtLimit) {
	super(balance, owner);
	if (debtLimit == null) {
	    throw new IllegalArgumentException("Debt limit can't be null.");
	} else {
	    this.debtLimit = debtLimit.setScale(2, RoundingMode.HALF_UP).abs();
	}
    }

    public BigDecimal getDebtLimit() {
	return debtLimit;
    }

    public void setDebtLimit(BigDecimal debtLimit) {
	if (debtLimit == null) {
	    throw new IllegalArgumentException("Debt limit can't be null.");
	} else {
	    this.debtLimit = debtLimit.setScale(2, RoundingMode.HALF_UP).abs();
	}
    }

    public void setDebtLimit(double debtLimit) {
	this.debtLimit = new BigDecimal(debtLimit).setScale(2, RoundingMode.HALF_UP).abs();
    }

    @Override
    public void decreaseBalance(BigDecimal decrement) {
	if (decrement == null) {
	    throw new IllegalArgumentException("Decrement can't be null.");
	} else {
	    BigDecimal balanceRemainder = balance.subtract(decrement).setScale(2, RoundingMode.HALF_UP);
	    if (balanceRemainder.compareTo(debtLimit.negate()) == -1) {
		throw new OutOfDebtLimitException("The withdrawal operation was rejected - "
			+ "there is not enough balance.");
	    } else {
		balance = balanceRemainder;
	    }
	}
    }

    @Override
    public void decreaseBalance(double decrement) {
	BigDecimal balanceRemainder = balance.subtract(new BigDecimal(decrement)).setScale(2, RoundingMode.HALF_UP);
	if (balanceRemainder.compareTo(debtLimit.negate()) == -1) {
	    throw new OutOfDebtLimitException("The withdrawal operation was rejected - there is not enough balance.");
	} else {
	    balance = balanceRemainder;
	}
    }
}
