package com.klimenkov;

import java.math.BigDecimal;
import com.klimenkov.exeptions.AbsentCardException;
import com.klimenkov.exeptions.InsertedCardException;

/**
 * Atm is a class that simulates the work of a simple ATM machine.
 * It can take only one card at a time and give it back.
 * When the card is inserted Atm provides operations of getting balance, 
 * withdrawal and depositing money operations.
 */
public class Atm {

    private Card card;

    public Card getCard() {
	return card;
    }

    public void insertCard(Card card) {
	if (this.card != null) {
	    throw new InsertedCardException("The ATM already has an inserted card.");
	} else {
	    if (card == null) {
		throw new IllegalArgumentException("Card can't be null.");
	    } else {
		this.card = card;
	    }
	}
    }

    public void removeCard() {
	if (card == null) {
	    throw new AbsentCardException("The ATM doesn't have an inserted card.");
	} else {
	    this.card = null;
	}	
    }

    public BigDecimal getBalance() {
	if (card == null) {
	    throw new AbsentCardException("The ATM doesn't have an inserted card.");
	} else {
	    return card.getBalance();
	}
    }

    public void depositMoney(BigDecimal increment) {
	if (card == null) {
	    throw new AbsentCardException("The ATM doesn't have an inserted card.");
	} else {
	    card.increaseBalance(increment);
	}
    }

    public void depositMoney(double increment) {
	if (card == null) {
	    throw new AbsentCardException("The ATM doesn't have an inserted card.");
	} else {
	    this.card.increaseBalance(increment);
	}
    }

    public void withdrawMoney(BigDecimal decrement) {
	if (card == null) {
	    throw new AbsentCardException("The ATM doesn't have an inserted card.");
	} else {
	    this.card.decreaseBalance(decrement);
	}
    }

    public void withdrawMoney(double decrement) {
	if (card == null) {
	    throw new AbsentCardException("The ATM doesn't have an inserted card.");
	} else {
	    this.card.decreaseBalance(decrement);
	}
    }
}
