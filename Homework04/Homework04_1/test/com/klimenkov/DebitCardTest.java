package com.klimenkov;

import org.junit.Test;
import org.junit.Assert;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class DebitCardTest {
    @Test
    public void testDecreaseBalance_withBigDecimal() {
	BigDecimal balance = new BigDecimal(199).setScale(2, RoundingMode.HALF_UP);
	BigDecimal decrement = new BigDecimal(00.99999).setScale(2, RoundingMode.HALF_UP);

	DebitCard card = new DebitCard("Elon Musk", balance);
	card.decreaseBalance(decrement);

	balance = balance.subtract(decrement).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getBalance());
    }

    @Test
    public void testDecreaseBalance_withDouble() {
	BigDecimal balance = new BigDecimal(420.00001).setScale(2, RoundingMode.HALF_UP);
	double decrement = 99.99999;

	DebitCard card = new DebitCard("Linus Torvalds", balance);
	card.decreaseBalance(decrement);

	balance = balance.subtract(new BigDecimal(decrement)).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getBalance());
    }
}
