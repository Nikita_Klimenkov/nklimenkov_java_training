package com.klimenkov;

import org.junit.Test;
import org.junit.Assert;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class CreditCardTest {
    @Test
    public void testGetDebtLimit_withBigDecimal() {
	BigDecimal balance = new BigDecimal(100.99).setScale(2, RoundingMode.HALF_UP);
	BigDecimal debtLimit = new BigDecimal(50).setScale(2, RoundingMode.HALF_UP);
	CreditCard card = new CreditCard("Bill Gates", balance, debtLimit);

	debtLimit = new BigDecimal(20.42).setScale(2, RoundingMode.HALF_UP);
	card.setDebtLimit(debtLimit);
	Assert.assertEquals(debtLimit, card.getDebtLimit());
    }

    @Test
    public void testGetDebtLimit_withDouble() {
	BigDecimal balance = new BigDecimal(99.99).setScale(2, RoundingMode.HALF_UP);
	BigDecimal debtLimit = new BigDecimal(42.009).setScale(2, RoundingMode.HALF_UP);
	CreditCard card = new CreditCard("Tim Cook", balance, debtLimit);

	debtLimit = new BigDecimal(43.01).setScale(2, RoundingMode.HALF_UP);
	card.setDebtLimit(43.01);
	Assert.assertEquals(debtLimit, card.getDebtLimit());
    }

    @Test
    public void testDecreaseBalance_withBigDecimal() {
	BigDecimal balance = new BigDecimal(199).setScale(2, RoundingMode.HALF_UP);
	BigDecimal decrement = new BigDecimal(00.99999).setScale(2, RoundingMode.HALF_UP);

	CreditCard card = new CreditCard("Elon Musk", balance, new BigDecimal(100));
	card.decreaseBalance(decrement);

	balance = balance.subtract(decrement).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getBalance());
    }

    @Test
    public void testDecreaseBalance_withDouble() {
	BigDecimal balance = new BigDecimal(420.00001).setScale(2, RoundingMode.HALF_UP);
	double decrement = 99.99999;

	CreditCard card = new CreditCard("Linus Torvalds", balance, new BigDecimal(66));
	card.decreaseBalance(decrement);

	balance = balance.subtract(new BigDecimal(decrement)).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getBalance());
    }
}
