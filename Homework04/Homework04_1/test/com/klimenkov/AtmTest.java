package com.klimenkov;

import org.junit.Test;
import org.junit.Assert;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class AtmTest {
    @Test
    public void testGetBalance_withCard() {
	BigDecimal balance = new BigDecimal(566.33).setScale(2, RoundingMode.HALF_UP);
	Card card = new Card("Mark Zuckerberg", balance);	
	Atm atm = new Atm();
	atm.insertCard(card);
	Assert.assertEquals(balance, atm.getBalance());
    }
    
    @Test
    public void testGetBalance_withCreditCard() {
	BigDecimal balance = new BigDecimal(100.99).setScale(2, RoundingMode.HALF_UP);
	BigDecimal debtLimit = new BigDecimal(49.99).setScale(2, RoundingMode.HALF_UP);
	CreditCard card = new CreditCard("Pavel Durov", balance, debtLimit);	
	Atm atm = new Atm();
	atm.insertCard(card);
	Assert.assertEquals(balance, atm.getBalance());
    }
    
    @Test
    public void testGetBalance_withDebitCard() {
	BigDecimal balance = new BigDecimal(333.33).setScale(2, RoundingMode.HALF_UP);	
	DebitCard card = new DebitCard("Elon Musk", balance);	
	Atm atm = new Atm();
	atm.insertCard(card);
	Assert.assertEquals(balance, atm.getBalance());
    }
}
