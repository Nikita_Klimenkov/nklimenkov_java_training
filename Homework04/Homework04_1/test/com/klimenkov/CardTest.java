package com.klimenkov;

import org.junit.Test;
import org.junit.Assert;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class CardTest {
    @Test
    public void testIncreaseBalance_withBigDecimal() {
	BigDecimal balance = new BigDecimal(100.001).setScale(2, RoundingMode.HALF_UP);
	BigDecimal increment = new BigDecimal(42.99).setScale(2, RoundingMode.HALF_UP);

	Card card = new Card("Bill Gates", balance);
	card.increaseBalance(increment);

	balance = balance.add(increment).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getBalance());
    }

    @Test
    public void testIncreaseBalance_withDouble() {
	BigDecimal balance = new BigDecimal(999.999999).setScale(2, RoundingMode.HALF_UP);
	double increment = 42.01;

	Card card = new Card("Tim Cook", balance);
	card.increaseBalance(increment);

	balance = balance.add(new BigDecimal(increment)).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getBalance());
    }

    @Test
    public void testDecreaseBalance_withBigDecimal() {
	BigDecimal balance = new BigDecimal(199).setScale(2, RoundingMode.HALF_UP);
	BigDecimal decrement = new BigDecimal(00.99999).setScale(2, RoundingMode.HALF_UP);

	Card card = new Card("Elon Musk", balance);
	card.decreaseBalance(decrement);

	balance = balance.subtract(decrement).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getBalance());
    }

    @Test
    public void testDecreaseBalance_withDouble() {
	BigDecimal balance = new BigDecimal(420.00001).setScale(2, RoundingMode.HALF_UP);
	double decrement = 99.99999;

	Card card = new Card("Linus Torvalds", balance);
	card.decreaseBalance(decrement);

	balance = balance.subtract(new BigDecimal(decrement)).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getBalance());
    }

    @Test
    public void testGetConvertedBalance_withBigDecimal() {
	BigDecimal balance = new BigDecimal(1000.333333333).setScale(2, RoundingMode.HALF_UP);
	Card card = new Card("Pavel Durov", balance);

	double conversionRate = 0.8999;
	balance = balance.multiply(new BigDecimal(conversionRate)).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getConvertedBalance(conversionRate));
    }

    @Test
    public void testGetConvertedBalance_withDouble() {
	BigDecimal balance = new BigDecimal(0.99).setScale(2, RoundingMode.HALF_UP);
	Card card = new Card("Mark Zuckerberg", balance);

	double conversionRate = 12.3456789;
	balance = balance.multiply(new BigDecimal(conversionRate)).setScale(2, RoundingMode.HALF_UP);
	Assert.assertEquals(balance, card.getConvertedBalance(conversionRate));
    }
}
